class Api {
    constructor () {
        this.url = 'https://api.themoviedb.org/3/search/movie?api_key=9ab4e0c0c4d3ba62a8ae20bc1aaa38f1&query=';
        this.headers = {"Content-Type": "application/json" };
    }

    //API KEY 9ab4e0c0c4d3ba62a8ae20bc1aaa38f1

    async fetchMoviesBySearchText (search) { 
        try {
            const response = await fetch(`${this.url}${search}`,{
                headers: this.headers
            });
            const data = await response.json();
            return data;
        } catch (err) {
            console.log(err);
        }
    }
}

const renderMovies = (fetchedFilms, listof) => {
    fetchedFilms.forEach( element => {
        
        const filmlist = document.createElement('li');

        filmlist.innerHTML = `
        ${element.original_title}
        `
        listof.append(filmlist);
    });
}

const search = document.querySelector('input');

search.addEventListener('keypress', async (evt) => {
    if (evt.key === 'Enter') {
    searchQuery = evt.target.value;
    search.value = '';
    const searchFilm = new Api();
    const result = await searchFilm.fetchMoviesBySearchText(searchQuery);

        const listof = document.querySelector('ul');
        if (result.total_results == 0) {
            listof.innerHTML = "";
            listof.append(`No results for ${searchQuery}`);
        } else {
            listof.innerHTML = "";
            listof.append(`Results: ${result.total_results}`);
            renderMovies(result.results, listof);
            console.log(result.total_pages);

            let page = 1;
            if (result.total_pages > page) {
                const loadMoreButton = document.createElement('button');
                loadMoreButton.textContent = "Load More";
                listof.append(loadMoreButton);
                
                loadMoreButton.addEventListener('click', async () => {
                    page = page + 1;
                    console.log(page);
                    const nextPage = await searchFilm.fetchMoviesBySearchText(`${searchQuery}&page=${page}`);
                    renderMovies(nextPage.results, listof);
                    listof.append(loadMoreButton);
		            if (nextPage.total_pages == page) {
		                loadMoreButton.remove();
		            }
                });
            } 
        }
    }
})